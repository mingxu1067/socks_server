#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <algorithm>

#define CONNECT_MODE 0x01
#define BIND_MODE 0x02

typedef struct {
    unsigned char VN;
    unsigned int CD;
    unsigned int DST_PORT;
    unsigned int DST_IP;
    unsigned char* USERID;
} SocksPackage;

SocksPackage unpackSocksPackage(unsigned char* buffer) {
    SocksPackage package;

    package.VN = buffer[0];
    package.CD = (unsigned int)buffer[1];
    package.DST_PORT = (unsigned char)(buffer[2]) << 8  | (unsigned char)(buffer[3]);
    package.DST_IP  =  (unsigned char)(buffer[7]) << 24 | (unsigned char)(buffer[6]) << 16 |
                         (unsigned char)(buffer[5]) << 8  | (unsigned char)(buffer[4]);

    package.USERID = buffer + 8;

    return package;
}

void proxyService(int fd);
void printSocksPackageInfo(SocksPackage* package);
bool isDSTPermission(int CD, unsigned int DST_IP);
void dataSwitch(int source_fd, int host_fd);

int main(int argc, char const *argv[]) {
    int port = 7000;

    for (int i = 1; i < argc; i++) {
        std::string para(argv[i]);
        if ((para.find("--port=") == 0) || (para.find("-p=") == 0)) {
            port = atoi(para.substr(para.find("=") + 1).c_str());
        }
    }

    int listen_fd;
    struct sockaddr_in listen_address;
    memset((char*)&listen_address, 0, sizeof(listen_address));

    listen_address.sin_family = AF_INET;
    listen_address.sin_addr.s_addr = htonl(INADDR_ANY);
    listen_address.sin_port = htons(port);

    int opt_value = 1;
    setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt_value, sizeof(int));

    if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "Fail to create socket.\n");
    }

    if(bind(listen_fd, (struct sockaddr *) &listen_address, sizeof(listen_address)) == -1) {
        fprintf(stderr, "Fail to bind socket.\n");
        exit(1);
    }

    if(listen(listen_fd, 0) == -1) {
        fprintf(stderr, "Fail to listen socket.\n");
        exit(1);
    }


    while (true) {
        int client_fd = -1;
        int client_len = 0;

        struct sockaddr_in client_addr;
        if (client_fd = accept(listen_fd, (struct sockaddr*) &client_addr, (socklen_t*)&client_len)) {
            pid_t pid = fork();
            if (pid == 0) {
                printf("=====Create child to serve the client %d=====\n", getpid());
                
                proxyService(client_fd);

                exit(0);

            } else {
                close(client_fd);
            }
        }
    }

    close(listen_fd);
    return 0;
}

void proxyService(int fd) {
    unsigned char buffer[512];
    read(fd, buffer, sizeof(buffer));

    SocksPackage package = unpackSocksPackage(buffer);
    printSocksPackageInfo(&package);
    fflush(stdout);
    if( package.VN != 0x04 ){
        printf("Error: SOCKs Version is not 4.\n");
        return;
    }

    socklen_t client_len;
    struct sockaddr_in client_addr;
    getpeername(fd, (struct sockaddr*)&client_addr, &client_len);

    unsigned int client_port = ntohs(client_addr.sin_port);
    unsigned int client_ip = ntohl(client_addr.sin_addr.s_addr);
    printf("<S_IP>: %d.%d.%d.%d\n", (client_ip >> 24) & 255, (client_ip >> 16) & 255,
                                    (client_ip >> 8) & 255, (client_ip & 255));
    printf("<S_PORT>: %d\n", client_port);

    if (package.CD == CONNECT_MODE) {
        printf("<COMMAND>: Connect\n");
    } else if (package.CD == BIND_MODE){
        printf("<COMMAND>: Bind\n");
    }

    unsigned char reply[8];
    reply[0] = package.VN;
    if (isDSTPermission(package.CD, package.DST_IP)) {
        reply[1] = 0x5A;
        printf("<Reply>: Accept\n");
    } else {
        reply[1] = 0x5B;
        printf("<Reply>: Reject\n");
    }
    reply[2] = package.DST_PORT / 256;
    reply[3] = package.DST_PORT % 256;
    reply[4] = (package.DST_IP >> 24) & 255;
    reply[5] = (package.DST_IP >> 16) & 255;
    reply[6] = (package.DST_IP >> 8)  & 255;
    reply[7] = package.DST_IP & 255;

    if (reply[1] == 0x5B) {
        fflush(stdout);
        write(fd, reply, 8);
        close(fd);
        return;
    }

    int host_fd;
    if (package.CD == CONNECT_MODE) {
        host_fd = socket(AF_INET, SOCK_STREAM, 0 );

        sockaddr_in dst_addr;
        dst_addr.sin_family = AF_INET;
        dst_addr.sin_addr.s_addr = package.DST_IP;
        dst_addr.sin_port = htons(package.DST_PORT);

        if( connect(host_fd, (struct sockaddr*)&dst_addr, sizeof(dst_addr)) < 0 ){
            printf("Error: Faild to connect host destination.\n");
            return;
        }

        reply[0] = 0;
        
        write(fd, reply, 8);
        dataSwitch(fd, host_fd);
        close(fd);
        close(host_fd);

    } else if (package.CD == BIND_MODE) {
        int bind_fd = socket(AF_INET, SOCK_STREAM, 0);

        sockaddr_in bind_addr;
        bind_addr.sin_family = AF_INET;
        bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        bind_addr.sin_port = htons(INADDR_ANY);

        int opt_value = 1;
        if (setsockopt(bind_fd, SOL_SOCKET, SO_REUSEPORT, (char *)&opt_value, sizeof(opt_value)) < 0) {
            printf("Error: Set Sock Error\n");
        }

        if ( bind(bind_fd, (sockaddr*)&bind_addr, sizeof(bind_addr)) < 0) {
            printf("Error: Bind Sock Error\n");
        }

        sockaddr_in host_port;
        int host_port_len = sizeof(host_port);
        if (getsockname(bind_fd, (sockaddr*)&host_port, (socklen_t*)&host_port_len) < 0) {
            printf("Error: Get Host Port Error\n");
        }
        if (listen(bind_fd, 5) < 0){
            printf("Error: Listen Error\n");
        }

        unsigned char buf[8];
        buf[0] = 0;
        buf[1] = 0x5A;
        buf[2] = (unsigned char)(ntohs(host_port.sin_port)/256);
        buf[3] = (unsigned char)(ntohs(host_port.sin_port)%256);
        buf[4] = 0;
        buf[5] = 0;
        buf[6] = 0;
        buf[7] = 0;
        write(fd, buf, 8);

        sockaddr_in remote_addr;
        int remote_len = sizeof(remote_addr);

        host_fd = accept(bind_fd, (sockaddr*)&remote_addr, (socklen_t*)&remote_len);
        
        write(fd, buf, 8);
        fflush(stdout);

        dataSwitch(fd, host_fd);
        close(fd);
        close(host_fd);
    } else {
        close(fd);
        return;
    }

}

void printSocksPackageInfo(SocksPackage* package) {
    printf("<VN>: %d\n<CD>: %d\n" ,package->VN, package->CD);
    printf("<D_IP> : %d.%d.%d.%d\n", (package->DST_IP >> 24) & 255, (package->DST_IP >> 16) & 255,
                                     (package->DST_IP >> 8) & 255, (package->DST_IP & 255));
    printf("<D_PORT>: %d\n", package->DST_PORT);
    printf("<USERID>: %s\n", package->USERID);
}

bool isDSTPermission(int CD, unsigned int DST_IP) {

    FILE* fp = fopen("socks.conf", "r" );
    if( fp == NULL ){
        printf("Error: socks.conf is not found.\n");
        return false;
    }

    char mode;
    if (CD == CONNECT_MODE) {
        mode = 'c';
    } else if (CD == BIND_MODE) {
        mode = 'b';
    } else {
        printf("Error: Connection mode is not wrong.\n");
        return false;
    }
    int all = '*';

    int x1 = (DST_IP >> 24 ) & 255;
    int x2 = (DST_IP >> 16 ) & 255;
    int x3 = (DST_IP >> 8 ) & 255;
    int x4 = DST_IP & 255;

    char str[128];
    bool isPermitted = false;
    while( fgets(str, 100, fp) != NULL ){
        char m;
        char ip[12 + 3 + 1];
        sscanf(str, "permit %c %s", &m, ip);

        int f1, f2, f3, f4;
        char* token = strtok(ip, ".");
        if (strcmp(token, "*") == 0) {
            f1 = all;
        } else {
            f1 = atoi(token);
        }

        token = strtok(NULL, ".");
        if (strcmp(token, "*") == 0) {
            f2 = all;
        } else {
            f2 = atoi(token);
        }

        token = strtok(NULL, ".");
        if (strcmp(token, "*") == 0) {
            f3 = all;
        } else {
            f3 = atoi(token);
        }

        token = strtok(NULL, ".");
        if (strcmp(token, "*") == 0) {
            f4 = all;
        } else {
            f4 = atoi(token);
        }

        if (m == mode) {
            if ((f1 == all) ||
                (f1 == x1 && f2 == all) ||
                (f1 == x1 && f2 == x2 && f3 == all) ||
                (f1 == x1 && f2 == x2 && f3 == x3 && f4 == all) || 
                (f1 == x1 && f2 == x2 && f3 == x3 && f4 == x4)) {
                isPermitted = true;
            }
        }

    }

    return isPermitted;
}

void dataSwitch(int source_fd, int host_fd) {
    fd_set rfds, rs;
    FD_ZERO(&rs);
    FD_SET(source_fd, &rs);
    FD_SET(host_fd, &rs);

    char buffer[32768];
    int nfds = std::max(host_fd, source_fd) + 1;

    while(true) {
        rfds = rs;
        select(nfds, &rfds, NULL, NULL,(struct timeval*)0);

        if (FD_ISSET(host_fd, &rfds)) {
            int n = read(host_fd, buffer, sizeof(buffer));
            if (n == 0 || n == -1){
                exit(0);
            } else{
                printf("<Content>: %d,%d,%d,%d,%d...\n", buffer[0], buffer[1], buffer[2], buffer[3], buffer[4]);
                n = write(source_fd, buffer, n);
            }
        }

        if (FD_ISSET(source_fd, &rfds)) {
            int n = read(source_fd, buffer, sizeof(buffer));
            if (n == 0 || n == -1) {
                exit(0);
            } else {
                printf("<Content>: %d,%d,%d,%d,%d...\n", buffer[0], buffer[1], buffer[2], buffer[3], buffer[4]);
                n = write(host_fd, buffer, n);
            }
        }
    }
}