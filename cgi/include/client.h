#ifndef RBS_CLIENT_H_
#define RBS_CLIENT_H_

#include <netinet/in.h>

#define BUFFER_SIZE 32768

class Client {
public:
    Client();
    ~Client();

    void setIp(char* ip);
    char* getIp();

    void setPort(char* port);
    char* getPort();

    void setFilename(char* filename);
    char* getFilename();

    void setProxyIp(char* ip);
    char* getProxyIp();

    void setProxyPort(char* port);
    char* getProxyPort();

    void setRequested(bool status);
    bool getRequested();

    int getSentedCount();
    char* getBuffer();

    bool isWriteEnd();
    void resetSentedCount();
    void addSentedCount(int count);

    char* getCurrentWriteBufferPosition();

    void printInfo();

private:
    char host_ip[INET_ADDRSTRLEN];
    char host_port[6];
    char host_batch_file[100];
    char proxy_ip[INET_ADDRSTRLEN];
    char proxy_port[6];
    bool requested;
    int client_sented_count;
    char client_write_buffer[BUFFER_SIZE];
};

#endif